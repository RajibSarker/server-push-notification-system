﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PushNotificationSystemEngine.Migrations
{
    public partial class updatedtheuserstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "PasswordHash",
                table: "Tbl_USERS",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "PasswordSalt",
                table: "Tbl_USERS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Tbl_USERS",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PasswordHash",
                table: "Tbl_USERS");

            migrationBuilder.DropColumn(
                name: "PasswordSalt",
                table: "Tbl_USERS");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Tbl_USERS");
        }
    }
}
