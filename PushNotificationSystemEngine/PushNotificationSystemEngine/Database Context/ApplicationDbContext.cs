﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using PushNotificationSystemEngine.Models;

namespace PushNotificationSystemEngine.Database_Context
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions options):base(options)
        {

        }

        // entities
        public DbSet<User> Users { get; set; }
        public DbSet<UserConnection> UserConnections { get; set; }

        public static ApplicationDbContext Create(IConfiguration configuration)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("AppConnectionString"));
            var context = new ApplicationDbContext(optionsBuilder.Options);
            return context;
        }
    }
}
