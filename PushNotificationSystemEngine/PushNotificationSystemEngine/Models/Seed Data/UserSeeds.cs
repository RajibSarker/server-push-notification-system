﻿using Newtonsoft.Json;
using PushNotificationSystemEngine.Database_Context;
using System.Collections.Generic;
using System.Linq;

namespace PushNotificationSystemEngine.Models.Seed_Data
{
    public class UserSeeds
    {
        public static void Seeds(ApplicationDbContext context)
        {
            if (!context.Users.Any())
            {
                var seedUsers = System.IO.File.ReadAllText("Models/Seed Data/UserSeedData.json");
                var uses = JsonConvert.DeserializeObject<List<User>>(seedUsers);
                foreach (var user in uses)
                {
                    byte[] passwordHash, passwordSalt;
                    CreatepasswordHash("password", out passwordHash, out passwordSalt);
                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;
                    user.UserName = user.UserName;
                    context.Users.Add(user);
                }

                context.SaveChanges();
            }
        }

        private static void CreatepasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
