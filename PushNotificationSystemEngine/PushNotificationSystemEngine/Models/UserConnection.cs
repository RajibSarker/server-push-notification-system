﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PushNotificationSystemEngine.Models
{
    [Table("Tbl_USER_CONNECTIONS")]
    public class UserConnection
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public string ConnectionId { get; set; }
        public bool IsActive { get; set; }
        public string BrowserInfo { get; set; }
    }
}
