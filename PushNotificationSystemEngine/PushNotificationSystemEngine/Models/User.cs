﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PushNotificationSystemEngine.Models
{
    [Table("Tbl_USERS")]
    public class User
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
    }
}
