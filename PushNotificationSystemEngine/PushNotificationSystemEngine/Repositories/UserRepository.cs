﻿using Microsoft.EntityFrameworkCore;
using PushNotificationSystemEngine.Database_Context;
using PushNotificationSystemEngine.Models.Dto;
using PushNotificationSystemEngine.Repositories.Interfaces;
using System.Threading.Tasks;

namespace PushNotificationSystemEngine.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _db;

        public UserRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<bool> Login(UserLoginDto userLoginDto)
        {
            if(userLoginDto == null) return false;

            var existingUser =
                await _db.Users.FirstOrDefaultAsync(c => c.UserName.ToLower() == userLoginDto.UserName.ToLower());
            if (existingUser == null)
            {
                return false;
            }

            if (!VerifyingHashPassword(userLoginDto.Password, existingUser.PasswordHash, existingUser.PasswordSalt))
            {
                return false;
            }

            return true;
        }

        private bool VerifyingHashPassword(string password, byte[] existingUserPasswordHash, byte[] existingUserPasswordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(existingUserPasswordSalt))
            {
                var computeHashPassword = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computeHashPassword.Length; i++)
                {
                    if (computeHashPassword[i] != existingUserPasswordHash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
