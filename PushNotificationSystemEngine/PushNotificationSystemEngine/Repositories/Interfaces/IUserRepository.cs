﻿using PushNotificationSystemEngine.Models.Dto;
using System.Threading.Tasks;

namespace PushNotificationSystemEngine.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<bool> Login(UserLoginDto userLoginDto);
    }
}
