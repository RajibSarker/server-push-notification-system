﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PushNotificationSystemEngine.Models.Dto;
using PushNotificationSystemEngine.Repositories.Interfaces;
using System.Threading.Tasks;

namespace PushNotificationSystemEngine.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginDto login)
        {
            if (login == null) return BadRequest("Invalid input request.");

            return Ok(await _userRepository.Login(login));
        }
    }
}
