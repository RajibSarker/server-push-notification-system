import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = 'https://localhost:5001/api/';

  constructor(private http: HttpClient) { }

  // login method
  login(model: any) {
    return this.http.post(this.baseUrl + 'users/login', model);
  }
}
