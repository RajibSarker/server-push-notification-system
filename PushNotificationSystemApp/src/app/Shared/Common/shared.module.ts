import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NavComponent } from "./nav/nav.component";

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule
    ],
    declarations: [
        NavComponent,
    ],
    exports: [
        NavComponent,
        BrowserModule,        
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule 
    ]
  })
  export class SharedModule { }
  