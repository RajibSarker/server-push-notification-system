import { Component } from '@angular/core';
import { UserService } from './_services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  model: any = {userName: '', password: ''};
  title = 'PushNotificationSystemApp';

  constructor(private userService: UserService){

  }

  login(){
    this.userService.login(this.model).subscribe(res=>{
      if(res){
        console.log('Logged in successful.');
      } else{
        console.log('User name or password does not match. Please try again later.');        
      }
    })
  }
}
